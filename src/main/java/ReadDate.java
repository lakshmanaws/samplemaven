import com.google.gson.Gson;
import DAO.Data;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ReadDate {

    public static Data data;

    public static Data readData() {

        Gson gson = new Gson();
        try {
            data = gson.fromJson(new FileReader("src/main/resources/data.json"), Data.class);
        } catch (FileNotFoundException err) {
            System.err.println(err);
        }
        return (data);
    }
}
